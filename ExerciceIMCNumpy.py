# Imports
import random
import csv
import numpy as np

# Liste de noms de personnes : 
noms = ['Ewen', 'Benjamin', 'Kenan', 'Noa', 'Alice', 'Hélène', 'Marien', 'Lucie', 'Alexis', 'Carla']

# Infos avec lesquelles on représente les personnes :
people = []
for name in noms:
    person = {}
    person['nom'] = name
    person['age'] = random.randint(6, 75)
    person['sexe'] = random.choice(['Homme', 'Femme'])
    person['taille'] = round(random.uniform(1.2, 2), 2)
    person['poids'] = round(random.uniform(20, 100), 2)
    people.append(person)

# Calcul de l'IMC
for person in people:
    person['IMC'] = round(person['poids'] / (person['taille'] ** 2), 2)

# Affichage du résultat
for person in people:
    print('Nom:', person['nom'])
    print('Age:', person['age'])
    print('Sexe:', person['sexe'])
    print('IMC:', person['IMC'])
    print('\n')

# Export du résultat en CSV
header = ['nom', 'age', 'sexe', 'taille', 'poids', 'IMC']
rows = []
for person in people:
    row = [person['nom'], person['age'], person['sexe'], person['taille'], person['poids'], person['IMC']]
    rows.append(row)

with open('people.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    writer.writerow(header)
    writer.writerows(rows)

# Ewen Bellahoues BIN 1